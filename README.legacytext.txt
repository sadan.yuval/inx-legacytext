Fix Legacy Text
===============

Inkscape extension for Inkscape 0.92 or later to restore original
baseline spacing of text in legacy documents.


Background
==========

Text layout in upcoming Inkscape 0.92 will have better support for the
CSS standard:
http://wiki.inkscape.org/wiki/index.php/Release_notes/0.92#Line_Spacing
These internal changes may cause differences in layout (line spacing) of
text in legacy documents created with older versions of Inkscape:
http://wiki.inkscape.org/wiki/index.php/Line_Height_Bugs

The extension 'Fix Legacy Text' can help easing the transition to 0.92.

The next bug-fix release in the Inkscape 0.92.x series (Inkscape 0.92.1)
will have internal routines to fix text properties in legacy files on
load.  Users won't have a need for this extension once 0.92.1 has been
released; developers may want to keep it around to test files which have
been opened with '--no-convert-text-baseline-spacing' (a new command
line option to disable the automatic changes on load).


Features
========

Basic features:
- Adjust line spacing and font size
- Fix blank lines (0.92.x)

Other optional features:
- Fix generic font names
- Make plain text editable (to support inserting newlines)

Alternative fix for line spacing:
- Split regular text into lines
If the text in the legacy file has not been edited yet, and the drawing
was not viewboxed or scaled on open (DPI change), then the tspan
coordinates are still those of the original layout in Inkscape 0.91 or
earlier, and in most cases can be used to position the individual lines
exactly.


Installation
============

ZIP archives of releases are available here:
https://gitlab.com/su-v/inx-legacytext/tags

Download and unpack the ZIP archive, then copy the two files
- fix_legacy_text.inx
- fix_legacy_text.py
into the user extensions directory (see 'Inkscape Preferences > System'
for the exact location) and relaunch Inkscape. The extension will be
listed in the 'Extensions' menu as 'Document > Fix Legacy Text'.


Basic Usage
===========

Select text objects with incorrect baseline spacing and apply the
extension (use live preview to check and (de-)select options as needed).

Selected objects will be processed recursively, and any text objects
found e.g. within nested groups will be modified as needed.
If the extension is applied without a current selection, all text
objects in the drawing (including defs, hidden and/or locked layers)
will be modified based on the chosen options.


Known issues
============

* Blank lines

In Inkscape 0.91, a blank line is given the line spacing of the previous
line (even if the font-size is different).
In Inkscape 0.92, blank lines are given zero spacing. A work around is
to add a space to each blank line.

Adding a space to a blank line exposes however a bug in Inkscape: the
space is removed on load and on paste (which loads the clipbboard SVG
snippet). This problem also affects extensions - a space added to a
blank line via extension script is removed when Inkscape loads the file
content returned by the script.

This extension at the moment inserts a 'NO-BREAK SPACE' (U+00A0) instead
of a space as a placeholder, in order to achieve the expected line
spacing of text objects (regular or flowed) with blank lines. A solution
depends on fixing the space issue internally in Inkscape.

* Flowed text

Preserving or restoring every aspect of the the original exact layout of
flowed or multi-line texts in legacy files are beyond the scope of this
extension, for example the changes introduced earlier with new CSS
features of the 'writing-modes' branch (merged in r14487):
https://sourceforge.net/p/inkscape/mailman/message/34575873/
The python-based script extension has neither access to inkscape's
internal font information, nor by default a (cross-platform) tested
dependency on python-bindings for font libraries used by Inkscape
(fontconfig, freetype, harfbuzz, pango) to extract any specific font
metrics per used font as needed.

Some of the changes introduced in rev 14935 (Use CSS 'strut' as minimum
value of CSS line box height) on the other hand can be partially
compensated by shuffling style properties of the outer and inner text
elements (which is within the scope of script-based extensions).


Source
======

The extension is developed and maintained in:
https://gitlab.com/su-v/inx-legacytext

The feature request to include this extensions in inkscape:
https://bugs.launchpad.net/inkscape/+bug/1652340


License
=======

GPL-2+
